import * as React from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { withTheme } from '@material-ui/core/styles';
import { MakeIconPickerMenu } from './HelpIcons';

class HelpEditor extends React.Component {
  constructor() {
    super();
    this.state = {
      name: '',
      title: '',
      subtitle: '',
      content: '',
      icon: '',
      iconPickerOpen: false,
      pickerTarget: null
    }
  }

  useStyles = () => {
    return {
      root: {
        width: '100%',
        marignTop: '1%',
        marginBottom: '1%'
      }
    }
  }

  isNameValid =() => {
    return !this.state.name.includes(' ');
  }

  handleTextChange = (event) => {
    this.setState({ ...this.state, [event.target.name]: event.target.value });
  }

  menuOpenClose = (event) => {
    this.setState({ ...this.state, iconPickerOpen: !this.state.iconPickerOpen, pickerTarget: this.state.iconPickerOpen ? null : event.currentTarget});
  }

  menuClick = (event) => {
    this.setState({ ...this.state, icon: event.currentTarget.id, iconPickerOpen: !this.state.iconPickerOpen, pickerTarget: this.state.iconPickerOpen ? null : event.currentTarget });
  }

 makeTextField = (props) => {
    const classes = this.useStyles();
    return (
      <div id={props.name + '-wrapper'} style={classes.root}>
        <TextField id={props.name} { ...props } style={classes.root} onChange={this.handleTextChange} />
      </div>
      );
  }

  render() {
    const classes = this.useStyles();
    const validation = this.isNameValid() ? {} : { error: true, helperText: 'The name must have no spaces in it  '};
    const nameFieldBase = { name:'name', label:'Help item name', variant:"outlined", value:this.state.name };
    const nameField = { ...nameFieldBase, ...validation };
    const titleField = { name:'title', label:'Help title', variant:"outlined", value:this.state.title };
    const subtitleField = { name:'subtitle', label:'Help subtitle', variant:"outlined", value:this.state.subtitle };
    const contentField = { name:'content', label:'Help Details', variant:"outlined", multiline:true, rows:4, value:this.state.content };
    const menu = MakeIconPickerMenu(this.menuClick, this.menuOpenClose, this.state.iconPickerOpen, this.state.pickerTarget);

    return(
      <div id='help-editor-wrapper' style={classes.root}>
        { this.makeTextField(nameField) }
        { this.makeTextField(titleField) }
        { this.makeTextField(subtitleField) }
        { this.makeTextField(contentField) }
        <div id='button-wrapper' style={{marginTop:'2%'}}>
          <Button variant="contained" onClick={this.menuOpenClose}>Set Icon</Button>
          <Button variant="contained">Add Help</Button>
          {menu}
        </div>
      </div>
    );
  }
}

export default withTheme(HelpEditor);
