import * as React from 'react'
import PropTypes from 'prop-types';
import Skeleton from '@material-ui/lab/Skeleton';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';
import { withTheme } from '@material-ui/core/styles';

/**
 * Component to render help details for the Help component
 */
class HelpDetails extends React.Component {

  useStyles = () => {
    return {
      root: {
        padding: '10px',
        minWidth: '275px',
        maxWidth: this.props.constrainWidth ? '400px' : '100%'
      }
    }
  }

  /**
   * Make an ID to put on an element.
   * Uses the ID provided in the props.
   * postFix - the value to combine with the ID in the props.
   * returns a value that can be used as an ID.
   */
  makeId = (postFix) => {
    return this.props.id + '-' + postFix;
  }

  /**
   * Make the avatar to put in the header card.
   * icon - the icon to put in the avatar.
   * returns - an avatar with the given icon in it, or null if theres no icon.
   */
  makeAvatar = (icon) => {
    if (icon !== undefined && icon !== null) {
      return <Avatar id={this.makeId('avatar')}>{icon}</Avatar>
    }

    return null;
  }

  /**
   * Make a card to render in the help for the user.
   * avatar    - the avatar to put in the header.
   * title     - the title to put in the header.
   * subheader - the subtitle to put in the header.
   * content   - the content of the card.
   * returns   - a card built with the given data.
   */
  makeCard = (avatar, title, subheader, content) => {
    const classes = this.useStyles();
    return (
      <Card id={this.makeId('card')} style={classes.root} variant='outlined'>
        <CardHeader
          id={this.makeId('card-header')}
          title={title}
          subheader={subheader}
          avatar={avatar}
        />
        <CardContent id={this.makeId('card-content')}>
          {content}
        </CardContent>
      </Card>
    );
  }

  /**
   * Make a card that shows only loading skeletons.
   */
  makeLoading = () => {
    const contentSkelly = <Skeleton variant='text' animation='wave'/>
    const content = <span>{contentSkelly}{contentSkelly}{contentSkelly}</span>

    return this.makeCard(this.makeAvatar(null), <Skeleton variant='text' animation='wave'/>, <Skeleton variant='text' animation='wave' width='60%'/>, content);
  }

  /**
   * Make an item to add to the card content.
   * item - the item to show the user.
   * id   - the id to give the item.
   */
  makeContentItem = (item, id) => {
    return <Typography id={id} key={id} variant='body2'>{item}</Typography>
  }

  /**
   * Make a card that shows the data passed in the props.
   */
  makeDetail = () => {
    const key = 'help-detail-line'
    const content = [];

    if (Array.isArray(this.props.content)) {
      this.props.content.forEach((item, i) => {
          content.push(this.makeContentItem(item, key + '-' + i));
      });
    } else {
      content.push(this.makeContentItem(this.props.content, key));
    }

    return this.makeCard(this.makeAvatar(this.props.icon), this.props.title, this.props.subtitle, content);
  }

  render() {
    return(this.props.loading ? this.makeLoading() : this.makeDetail());
  }
}

HelpDetails.defaultProps = {
  id : 'help-details',
  title : '',
  subtitle: '',
  content: '',
  constrainWidth: true
};

HelpDetails.propTypes = {
  /**
   * The id to give this control and its children.
   */
  id : PropTypes.string,
  /**
   * The title for the help.
   */
  title : PropTypes.string,
  /**
   * Any subtitle to put in the header below the title.
   */
  subtitle : PropTypes.string,
  /**
   * The content of the help.
   */
  content : PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
  /**
   * Any icon to display in the header to the left of the title and subtitle.
   */
  icon : PropTypes.object,
  /**
   * Should this control be displayed in loading mode for user feedback or not.
   */
  loading : PropTypes.bool.isRequired,
  /**
   * Should the width of the help be constrained or not.
   * Defaults to true.
   */
  constrainWidth : PropTypes.bool
};

export default withTheme(HelpDetails);
