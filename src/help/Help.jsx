import * as React from 'react'
import PropTypes from 'prop-types';
import Tooltip from '@material-ui/core/Tooltip';
import HelpOutline from '@material-ui/icons/HelpOutline';
import IconButton from '@material-ui/core/IconButton';
import Popover from '@material-ui/core/Popover';
import Drawer from '@material-ui/core/Drawer';
import { withTheme } from '@material-ui/core/styles';
import { getHelpById, loadingHelpDetails, makeHelpDetailsTimeout } from './actions';
import HelpDetails from './HelpDetails';

/**
 * Component to display help.
 */
class Help extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      content : loadingHelpDetails,
      anchor  : null,
      loading : false,
      loaded  : false
    }
  }

  componentDidMount() {
    this.setContent(this.props.content);
  }

  componentDidUpdate(prevProps) {
    if (this.props.drawerMode !== prevProps.drawerMode) {
        this.reload();
    }

    if (this.props.content !== prevProps.content){
      this.populateHelp(this.props.content);
    }

    if (this.props.content && !this.isOpen() && !(this.state.loading || this.state.loaded)){
      this.populateHelp(this.props.content);
    } else if (this.isOpen()) {
      if (!this.state.loaded && !this.state.loading) {
        getHelpById(this.props.helpId, this.populateHelp, this.timeout, !this.props.drawerMode);
        this.setState({loading:true});
      }
    }

    if (this.props.drawerMode !== prevProps.drawerMode) {
      this.setState({content: this.resizeHelpDetails(this.state.content)});
    }
  }

  /**
   * Populate the help with the given value.
   * value - the thing to set as the help content.
   */
  populateHelp = (value) => {
    this.setState({
        content: this.resizeHelpDetails(value),
        loaded:true,
        loading:false
      });
  }

  /**
   * Set the help to the timeout help if the loading flags are set.
   */
  timeout = () => {
    if (!this.state.loaded && this.state.loading) {
      const timeoutHelp = makeHelpDetailsTimeout(this.reload, !this.props.drawerMode);
      this.setState({
          content : timeoutHelp,
          loading : false,
          loaded  : true
        });
    }
  }

  /**
   * Update the states to force a reload of the help.
   */
  reload = () => {
    this.setState({
        content : loadingHelpDetails,
        loading : false,
        loaded  : false
      });
  }

  /**
   * Resize help details to fit in with the constrainst that this help has.
   * object - the object to resize.
   * returns - a new help details object with a resized width or the original object.
   */
  resizeHelpDetails = (object) => {
    if (!object || !object.type || !object.type.displayName) {
      return object;
    }

    const displayName = object.type.displayName;
    if (displayName !== 'HelpDetails' && !displayName.includes('(HelpDetails)')) {
      return object;
    }

    const props = object.props;
    const constraint = {constrainWidth: this.props.drawerMode ? false : true};
    const newProps = {...props, ...constraint};

    return <HelpDetails {...newProps}/>
  }

  /**
   * Set the current state to the given value or loading help details.
   * value - the value to test and use if defined.
   */
  setContent = (value) => {
    const content = value !== undefined && value !== null ? value : loadingHelpDetails;
    this.setState({content: content});
  }

  /**
   * Make an ID to put on an element.
   * Uses the ID provided in the props.
   * postFix - the value to combine with the ID in the props.
   * returns a value that can be used as an ID.
   */
  makeId = (postFix) => {
    return this.props.id + '-' + postFix;
  }

  /**
   * Click handler to identify where to anchor the help to.
   * e - the click event, this will provide the element to anchor the help popup to.
   */
  handleClick = (e) => {
    if (this.state.anchor === null) {
      this.setState({anchor : this.props.drawerMode ? 'bottom': e.currentTarget});
    }
  }

  /**
   * Close handler to clear down the anchor.
   */
  handleClose = () => {
    if (this.state.anchor !== null) {
      this.setState({anchor : null});
    }
  }

  /**
   * Make a button for the user to click on to get help.
   * returns - a button with a tooltip for the user.
   */
  makeHelpButton = () => {
    const iconStyle = {
      padding:'0px',
      width:'18px',
      height:'18px'
    };

    const buttonStyle = {...this.props.style, ...iconStyle};
    const content = this.props.iconMode ? this.props.children : null;
    return (
            <span id={this.makeId('content-wrapper')}>
              {content}
              <Tooltip id={this.makeId('tooltip')} title='Click for Help' arrow>
                <IconButton id={this.makeId('button')} aria-label='help' style={buttonStyle} onClick={this.handleClick}>
                  <HelpOutline id={this.makeId('icon')} style={iconStyle}/>
                </IconButton>
              </Tooltip>
            </span>
    );
  }

  /**
   * Make a wrapper around any child elements so a tooltip help can be anchored to something.
   * returns - the children wrapped in a span that will control the tooltip help.
   */
  makeHelpWrapper =() => {
    return <span id={this.makeId('help-tooltip')} onMouseEnter={this.handleClick}>
            {this.props.children}
          </span>
  }

  /**
   * Are the help details being shown or not.
   * returns - true if the help details are being shown, false if not.
   */
  isOpen = () => {
    return this.state.anchor !== null;
  }

  render() {
    if (!this.props.display) {
      return null;
    }

    const tooltipMode = this.props.children && !this.props.iconMode;
    const helper = tooltipMode ? this.makeHelpWrapper() : this.makeHelpButton();
    const popoverOrigin = tooltipMode ? {vertical: 'bottom', horizontal: 'left'} : {vertical: 'top', horizontal: 'left'};
    const transformOrigin = tooltipMode ? {vertical: 'top', horizontal: 'left'} : {vertical: 'top', horizontal: 'left'};

    const display = this.props.drawerMode ?
                            <Drawer id={this.makeId('drawer')} anchor='bottom' open={this.isOpen()} onClose={this.handleClose}>{this.state.content}</Drawer>
                          :
                            <Popover
                              id={this.makeId('popover')}
                              open={this.isOpen()}
                              onClose={this.handleClose}
                              anchorEl={this.state.anchor}
                              anchorOrigin={popoverOrigin}
                              transformOrigin={transformOrigin}
                              disableRestoreFocus
                            >
                                {this.state.content}
                            </Popover>

    return(
        <span id={this.props.id}>
          {helper}
          {display}
        </span>
    );
  }
}

Help.defaultProps = {
  id: 'context-help',
  display: true,
  iconMode: false,
  helpId: null,
  style: {},
  content: null,
  drawerMode: false
};

Help.propTypes = {
  /**
   * The id to give this control and its children.
   */
  id: PropTypes.string,
  /**
   * Should this be displayed or not.
   * Default : true
   */
  display: PropTypes.bool,
  /**
   * Should the help be displayed as an icon to click on.
   * Use this when this control has been wrapped around other controls like a tooltip.
   * Default : false
   */
  iconMode: PropTypes.bool,
  /**
   * The id of the help to load.
   */
  helpId: PropTypes.string.isRequired,
  /**
   * Any style to apply.
   */
  style: PropTypes.object,
  /**
   * The content to display in the help.
   * If this is populated nothing will be requested using the helpId.
   */
  content: PropTypes.object,
  /**
   * Should the help be shown in a popup drawer.
   * Defaults to false.
   */
  drawerMode : PropTypes.bool,
  /**
   * Any child nodes.
   * This should only be populated when this control has been wrapped around another on.
   */
  children: PropTypes.node
};

export default withTheme(Help);
