import React from 'react'
import request from 'superagent';
import HelpDetails from './HelpDetails';
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline';
import AccessTimeIcon from '@material-ui/icons/AccessTime';
import ReplayIcon from '@material-ui/icons/Replay';
import Button from '@material-ui/core/Button';

// help details that are just loading
export const loadingHelpDetails = <HelpDetails loading={true}/>

const timeoutMilliseconds = 10 * 1000;

/**
 * Load the help for a given ID.
 * helpId   - the ID of the help to load.
 * callback - the function to use to set the help for the application.
 */
export function getHelpById(helpId, callback, timoutFunction, constrainWidth) {
  request
    .get('http://localhost:3333/help')
    .query(helpId)
    .timeout({response: timeoutMilliseconds})
    .end((err, resp) => {
        if (err) {
          console.log(err);
          if (err.timeout) {
            timoutFunction();
          } else if (!err.status) {
            callback(makeHelpDetailsError(err.message, helpId, constrainWidth));
          } else {
            const error = err.rawResponse || err.message || '';
            callback(makeHelpDetailsError(error, helpId, constrainWidth));
          }
        } else {
          const data = resp.body;
          callback(makeHelpDetails(data, constrainWidth));
        }
    });
}

/**
 * Make a help details element from the given data.
 * helpData - the help data to populate into the help details element.
 * returns - the help details or null if the data is unusable.
 */
function makeHelpDetails(helpData, constrainWidth) {
  if(helpData && helpData.hasOwnProperty('content') && helpData.hasOwnProperty('title') && helpData.hasOwnProperty('id')) {
    const content = helpData.content.split('\n');
    const contentElements = [];

    content.forEach((item, i) => {
      contentElements.push(<span key={i}>{item}</span>)
    });


    return  <HelpDetails id={helpData.id} title={helpData.title} subtitle={helpData.subtitle} content={contentElements} loading={false} constrainWidth={constrainWidth}/>
  }

  return null;
}

/**
 * Make an error help details element.
 * error  - the error that occured.
 * helpId - the ID of the help that was being loaded when the error occored.
 * returns - a help details with the error in it.
 */
function makeHelpDetailsError(error, helpId, constrainWidth) {
  const content = [<span key={'helpId'}>{'Tried to load help item : ' + helpId}</span>, <span key={'error'}>{error}</span>];
  return <HelpDetails title={'Unable to load the help details'} content={content} icon={<ErrorOutlineIcon/>} loading={false} constrainWidth={constrainWidth}/>
}

/**
 * Make a help details for the case when a timeout occurs.
 * realoadFunction - the function to call if a reload is requested.
 * returns - help details with a reload button in.
 */
export function makeHelpDetailsTimeout(realoadFunction, constrainWidth) {
  const buttonPositionStyle={
      marginRight: 0,
      marginLeft: '65%',
      marginTop: '5%'
  };

  const realodButton = <Button
                          id='help-timeout-reload-button'
                          variant='contained'
                          color='secondary'
                          size='small'
                          endIcon={<ReplayIcon/>}
                          onClick={realoadFunction}
                           style={buttonPositionStyle}
                        >
                          Retry
                        </Button>

  const content = ['The help took too long to load.', realodButton];

  return <HelpDetails title={'Timed Out'} subtitle={'Help cannot be displayed'} content={content} icon={<AccessTimeIcon/>} loading={false} constrainWidth={constrainWidth}/>
}
