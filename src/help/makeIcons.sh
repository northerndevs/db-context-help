#!/bin/sh

# get the top of the HelpIcons.jsx, everything before the icon list
TOP=$(grep -n "const helpIconsDefinitions" HelpIcons.jsx |cut -d':' -f1)
head -$TOP HelpIcons.jsx

# assuming you have material ui in your git directory, if this is not where you put it change it
cd ~/git/material-ui/packages/material-ui-icons/src

# get all the paths (svg content) from the jsavascript files, remover the react fragment tags from the paths and store the unique values
grep "<path" *.js | cut -d' ' -f2-1024 | sed 's/React.Fragment//g' | sed 's/<>//g' | sed 's/<\/>//g' |sort -u > /tmp/paths
# find all the files that have the uniqie values in them
grep -F -f /tmp/paths ./*.js | cut -d: -f1 | cut -d'/' -f2 |cut -d. -f1 > /tmp/filelist

# get the names of the controls to importexport
# remove rounded and sharp versions of the icons as they make little to no difference to the icon variations
DATA=$(grep -F -f /tmp/filelist index.js | cut -d' ' -f5 | grep -v Rounded | grep -v Sharp | sort -d)

# loop through all the controls and out put them as their lower case name as :
# "<control>" : Icons.<control>,
echo "{"
for n in $DATA
 do
   value=$(echo $n |tr '[:upper:]' '[:lower:]')
   echo '"'$value'"'":Icons."$n","
 done
echo "}"
