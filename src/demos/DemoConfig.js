import React from 'react';
import { withTheme } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import FreeBreakfast from '@material-ui/icons/FreeBreakfast';
import Paper from '@material-ui/core/Paper';
import Help from '../help/Help';
import HelpDetails from '../help/HelpDetails';
import Grid from '@material-ui/core/Grid';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormGroup from '@material-ui/core/FormGroup';
import Switch from '@material-ui/core/Switch';
import TextField from '@material-ui/core/TextField';
import { useStyles, makeExplantion } from './Common';

function DemoConfig() {
  const [state, setState] = React.useState({
    iconMode: true,
    drawerMode: false,
    avatar: false,
    title: '',
    subtitle: '',
    content: '',
    hepId: '',
    loading : false
  });

  const classes = useStyles();

  const handleSwitchChange = event => {
    setState({ ...state, [event.target.name]: event.target.checked });
  };

  const textValue = event => {
    setState({ ...state, [event.target.name]: event.target.value });
  }

  const icon = state.avatar ? <FreeBreakfast/> : undefined;

  const helpDetails =  <HelpDetails
                            loading={state.loading}
                            title={state.title}
                            subtitle={state.subtitle}
                            icon={icon}
                            constrainWidth={state.drawerMode}
                            content={state.content}
                        />

  const helpId = state.helpId && state.helpId !== '' ? state.helpId : 'demo';

  const iconModeSwitch   = <Switch checked={state.iconMode}   onChange={handleSwitchChange} name="iconMode"   />
  const drawerModeSwitch = <Switch checked={state.drawerMode} onChange={handleSwitchChange} name="drawerMode" />
  const avatarSwitch     = <Switch checked={state.avatar}     onChange={handleSwitchChange} name="avatar"     />
  const loadingSwitch    = <Switch checked={state.loading}    onChange={handleSwitchChange} name="loading"    />

  const helpIdText    = <TextField name="helpId"    label="Help ID"   value={state.helpId}    onChange={textValue}/>
  const titleText     = <TextField name="title"     label="Title"     value={state.title}     onChange={textValue}/>
  const subtitleText  = <TextField name="subtitle"  label="Subtitle"  value={state.subtitle}  onChange={textValue}/>
  const contentText   = <TextField name="content"   label="Content"   value={state.content}   onChange={textValue}/>

  const helpDetailConfigGroup = (items) => <Grid item xs={6}><FormGroup>{items}</FormGroup></Grid>
  const controlLabel = (control, label) => <FormControlLabel control={control}  label={label} />

  const avatarControl  = controlLabel(avatarSwitch,     'Help Avatar');
  const loadingControl = controlLabel(loadingSwitch,    'Help loading');
  const iconControl    = controlLabel(iconModeSwitch,   'Icon Mode');
  const drawerControl  = controlLabel(drawerModeSwitch, 'Drawer Mode');

  const helpSwitches       = helpDetailConfigGroup(<div>{iconControl}{drawerControl}</div>);
  const helpDetailSwitches = helpDetailConfigGroup(<div>{avatarControl}{loadingControl}</div>);
  const helpDetailValues   = helpDetailConfigGroup(<div>{titleText}{subtitleText}{contentText}</div>);

  const standardPaper = (content) => <Paper className={classes.paper} elevation={3}>{content}</Paper>

  const helpConfig = standardPaper(<div>
                                    {makeExplantion('Configure how the help control works.')}
                                    {helpSwitches}
                                    <div className={classes.root}>
                                      {helpIdText}
                                      {makeExplantion('The Help ID is the thing requested from the server.')}
                                    </div>
                                  </div>);

  const contentConfig = standardPaper(<div>
                                        <Grid item xs={12}>{makeExplantion('Configure the help thats going to be presented.')}</Grid>
                                        <Grid container className={classes.container} spacing={2} justify="center">
                                          {helpDetailSwitches}
                                          {helpDetailValues}
                                        </Grid>
                                        <Grid item xs={12}>{makeExplantion('Preview of the above settings')}{helpDetails}</Grid>
                                      </div>);

  const helpPreview = standardPaper(<div>
                                      <Help id='demo-test' helpId={helpId} content={helpDetails} iconMode={state.iconMode} drawerMode={state.drawerMode}>
                                        <Button variant="contained" className={classes.button} color="secondary">Demo Button</Button>
                                      </Help>
                                      <div className={classes.root}>
                                        {makeExplantion('This help is configured using the settings to the left.')}
                                        {makeExplantion('The help control is wrapped around button.')}
                                      </div>
                                    </div>);

  return (
    <div className={classes.root}>
      <Grid container className={classes.container} spacing={2} justify="center">
        <Grid item xs={4}>{helpConfig}</Grid>
        <Grid item xs={4}>{contentConfig}</Grid>
        <Grid item xs={4}>{helpPreview}</Grid>
      </Grid>
    </div>
  );
}

export default withTheme(DemoConfig);
