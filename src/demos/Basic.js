import React from 'react';
import { withTheme } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Help from '../help/Help';
import HelpDetails from '../help/HelpDetails';
import { useStyles, makeExplantion } from './Common';

function App() {
  const classes = useStyles();
  const details = 'Stuff goes here\nIt should be able to take anything\n..as long as its in a string';
  const secondary = <HelpDetails loading={false} title='This is a button' subtitle='Don`t you know' content={details} constrainWidth={false}/>

  return (
    <div className={classes.root}>
      <Paper className={classes.paper} elevation={3}>
        <Button variant="contained" color="secondary" className={classes.button}>Secondary</Button>
        <Help id='loaded-test' helpId='secondary' content={secondary}/>
        {makeExplantion('This help is an example of loaded data.')}
        {makeExplantion('The help control is added after the button.')}
      </Paper>
    </div>
  );
}

export default withTheme(App);
