import React from 'react';
import { withTheme } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import NewReleases from '@material-ui/icons/NewReleases';
import Paper from '@material-ui/core/Paper';
import Help from '../help/Help';
import HelpDetails from '../help/HelpDetails';
import { useStyles, makeExplantion } from './Common';

function WrappedIcon() {
  const classes = useStyles();
  const tooltip =<HelpDetails loading={false} title='Look a tooltip' subtitle='Not your common or garden verity' content='Could use it this way too....' icon={<NewReleases/>} />

  return (
    <div className={classes.root}>
      <Paper className={classes.paper} elevation={3}>
        <Help id='wrapped-icon' helpId='tooltip' content={tooltip} iconMode={true}>
          <Button variant="contained" className={classes.button} color="primary">Wrapped Help</Button>
        </Help>
        {makeExplantion('This help is constructed the same way as the tooltip version, but with the iconMode turned on.')}
        {makeExplantion('The help control is wrapped around button.')}
      </Paper>
    </div>
  );
}

export default withTheme(WrappedIcon);
