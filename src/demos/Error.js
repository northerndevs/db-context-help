import React from 'react';
import { withTheme } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Help from '../help/Help';
import { useStyles, makeExplantion } from './Common';

function Error() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Paper className={classes.paper} elevation={3}>
        <Button variant="contained" className={classes.button}>Error</Button>
        <Help id='avatar-test' helpId='error'/>
        {makeExplantion('This help is is an example of one with an avatar.')}
        {makeExplantion('The help control is added after the button.')}
      </Paper>
    </div>
  );
}

export default withTheme(Error);
