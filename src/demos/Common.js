import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

export const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    minWidth:'200px'
  },
  paper: {
    margin : '10px',
    padding : '5%'
  },
  item: {
    minHeight : '220px',
    minWidth : '220px'
  },
  button: {
    margin: '10px'
  }
}));

export function makeExplantion(explanation){
  return (
    <Typography
      variant='body2'
      style={{padding: '3%'}}
    >
      {explanation}
    </Typography>
);};
