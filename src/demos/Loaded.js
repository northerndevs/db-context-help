import React from 'react';
import { withTheme } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Help from '../help/Help';
import { useStyles, makeExplantion } from './Common';

function Loaded() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Paper className={classes.paper} elevation={3}>
        <Button variant="contained" color="primary" className={classes.button}>Primary</Button>
        <Help id='loading-test' helpId='primary' style={{marginLeft:'-8px'}}/>
        {makeExplantion('This help is loading, note the animated lines for feedback. This help will load from the server (this has been given an intentional 5 second delay)')}
        {makeExplantion('The help control is added after the button.')}
      </Paper>
    </div>
  );
}

export default withTheme(Loaded);
