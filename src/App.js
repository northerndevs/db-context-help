import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { withTheme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

import DemoConfig from './demos/DemoConfig';
import Loaded from './demos/Loaded';
import Basic from './demos/Basic';
import Error from './demos/Error';
import WrappedTooltip from './demos/WrappedTooltip';
import WrappedIcon from './demos/WrappedIcon';
import HelpEditor from './help/HelpEditor';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  paper: {
    margin : '10px',
    padding : '5%'
  },
  item: {
    minHeight : '220px'
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  }
}));

function App() {
  const classes = useStyles();

  const expansionSummary = (text) =>  <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                                        <Typography className={classes.heading}>{text}</Typography>
                                      </ExpansionPanelSummary>

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu"><MenuIcon /></IconButton>
          <Typography variant="h6" className={classes.title}>Contex Help Demo</Typography>
          <Button color="inherit">A Button</Button>
        </Toolbar>
      </AppBar>
      <Paper className={classes.paper} elevation={3}>
      <ExpansionPanel>
          {expansionSummary('Fixed Demos')}
         <ExpansionPanelDetails>
           <Grid container className={classes.root} spacing={4} justify="space-evenly">
             <Grid item xs={3}><Loaded /></Grid>
             <Grid item xs={3}><Basic /></Grid>
             <Grid item xs={3}><Error /></Grid>
             <Grid item xs={4}><WrappedTooltip /></Grid>
             <Grid item xs={4}><WrappedIcon /></Grid>
           </Grid>
         </ExpansionPanelDetails>
       </ExpansionPanel>
       <ExpansionPanel>
          {expansionSummary('Configurable Demos')}
          <ExpansionPanelDetails>
            <DemoConfig />
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <ExpansionPanel>
           {expansionSummary('Help Editor')}
           <ExpansionPanelDetails>
             <HelpEditor />
           </ExpansionPanelDetails>
         </ExpansionPanel>
      </Paper>
    </div>
  );
}

export default withTheme(App);
